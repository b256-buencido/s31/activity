/*

1. What directive is used by Node.js in loading the modules it needs?

Answer:
require() method

2. What Node.js module contains a method for server creation?

Answer:
"http" module

3. What is the method of the http object responsible for creating a server using Node.js?

Answer:
createServer(function(request, response)) {} method

4. What method of the response object allows us to set status codes and content types?

Answer:
writeHead(HTTP Status Code, Data Type of response) method

5. Where will console.log() output its contents when run in Node.js?

Answer:
standard output stream of the console or terminal

6. What property of the request object contains the address's endpoint?

Answer:
URL path and query string of the request

*/